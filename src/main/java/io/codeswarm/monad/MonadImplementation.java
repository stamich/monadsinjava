package io.codeswarm.monad;

import java.util.function.Function;

public class MonadImplementation<T> implements Monad<T>{
    private final T value;

    // Constructor to encapsulate the value
    private MonadImplementation(T value) {
        this.value = value;
    }

    // Static 'of' method (also called 'unit') to wrap a value in a Monad
    public static <T> Monad<T> of(T value) {
        return new MonadImplementation<>(value);
    }

    // 'flatMap' method (also called 'bind') to apply a function that returns a Monad
    public <U> Monad<U> flatMap(Function<T, Monad<U>> mapper) {
        return mapper.apply(value);  // Apply the function to the encapsulated value
    }

    // 'map' method to apply a function and return a Monad (useful for simple transformations)
    public <U> Monad<U> map(Function<T, U> mapper) {
        return new MonadImplementation<>(mapper.apply(value));
    }

    // Get the value (mainly for testing and demonstration purposes)
    public T getValue() {
        return value;
    }

    @Override
    public String toString() {
        return "Monad(" + value + ")";
    }
}
