package io.codeswarm.monad;

import java.util.function.Function;

public interface Monad<T> {

        <U> Monad<U> flatMap(Function<T, Monad<U>> f);
        <U> Monad<U> map(Function<T, U> f);
        T getValue();
}
