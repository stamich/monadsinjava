package io.codeswarm.monad;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class MonadTest {

    @Test
    void testMonadOf() {
        Monad<Integer> monad = MonadImplementation.of(5);
        assertEquals(5, monad.getValue());
    }

    @Test
    void testMonadMap() {
        Monad<Integer> monad = MonadImplementation.of(10);
        Monad<Integer> result = monad.map(val -> val * 2);

        assertEquals(20, result.getValue());
    }

    @Test
    void testMonadFlatMap() {
        Monad<Integer> monad = MonadImplementation.of(10);
        Monad<Integer> result = monad.flatMap(val -> MonadImplementation.of(val * 2));

        assertEquals(20, result.getValue());
    }

    @Test
    void testMonadChainingWithFlatMap() {
        Monad<Integer> monad = MonadImplementation.of(5);
        Monad<Integer> result = monad
                .flatMap(val -> MonadImplementation.of(val * 2))
                .flatMap(val -> MonadImplementation.of(val + 10));

        assertEquals(20, result.getValue());
    }

    @Test
    void testMonadToString() {
        Monad<String> monad = MonadImplementation.of("Hello");
        assertEquals("Monad(Hello)", monad.toString());
    }
}
